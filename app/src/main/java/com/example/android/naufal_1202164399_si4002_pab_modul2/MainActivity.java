package com.example.android.naufal_1202164399_si4002_pab_modul2;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView jumlahTiket ,topup, tanggalpp, jampp,jumlahSaldo,waktuview,
            tanggalview,tanggalviewpp,waktuviewpp,selamat;
    Button beli;
    Spinner spinner;
    String lokasiTujuan,top;
    Switch switchpp;
    private EditText input;
    private int mYear, mMonth, mDay, mHour, mMinute;
    int tops, totalHarga;
    DatePickerDialog datePicker;
    Calendar cl;
    EditText inputJumlah;
    int saldo = 0;
    int harga = 0;
    int jumlah = 0;
    String strHarga = "0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //button waktu dan tanggal
        jampp = (TextView)findViewById(R.id.waktuPP);
        switchpp = (Switch)findViewById(R.id.switchPP);
        topup = (TextView)findViewById(R.id.topUp);
        jumlahSaldo = (TextView)findViewById(R.id.jumlahSaldo);
        tanggalview = (TextView)findViewById(R.id.tanggal);
        waktuview = (TextView)findViewById(R.id.waktu);
        jampp = (TextView) findViewById(R.id.waktuPP);
        tanggalpp = (TextView) findViewById(R.id.tanggalPP);
        beli = (Button)findViewById(R.id.buttonBeli);
        inputJumlah = (EditText) findViewById(R.id.inputJumlah);
        tanggalviewpp = (TextView) findViewById(R.id.tanggalPP);
        waktuviewpp = (TextView) findViewById(R.id.waktuPP);
        selamat = (TextView) findViewById((R.id.ucapanSelamat));
        selamat.setText(getIntent().getStringExtra("ucapan"));
        waktuviewpp.setClickable(false);
        tanggalviewpp.setClickable(false);
        beli.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                harga = Integer.parseInt(strHarga);
                if (inputJumlah.getText().toString().length()> 0 ) {
                    jumlah = Integer.parseInt(inputJumlah.getText().toString());
                }else{
                    jumlah = 0;
                }
                if (jumlahSaldo.getText().equals("0")) {
                    Toast.makeText(MainActivity.this, "Silahkan Top Up dan pilih tujuan", Toast.LENGTH_SHORT).show();;
                }else if(lokasiTujuan == null){
                    Toast.makeText(MainActivity.this, "Silahkan pilih tujuan", Toast.LENGTH_SHORT).show();
                }else if( saldo <(jumlah * harga)){
                    Toast.makeText(MainActivity.this, "Silahkan Top Up", Toast.LENGTH_SHORT).show();
                }else {
                    if(switchpp.isChecked()){
                        totalHarga = (2*jumlah*harga);
                        saldo = (saldo - totalHarga);
                        jumlahSaldo.setText(String.valueOf(saldo));
                    }else{
                        totalHarga = (jumlah*harga);
                        saldo = (saldo - totalHarga);
                        jumlahSaldo.setText(String.valueOf(saldo));
                    }
                    String berangkat =tanggalview.getText().toString() +"-"+waktuview.getText().toString() ;
                    String pulang = tanggalviewpp.getText().toString()+"-"+waktuviewpp.getText().toString();
                    Intent in = new Intent(MainActivity.this,checkoutActivity.class);
                    in.putExtra("TotalHarga",String.valueOf(totalHarga));
                    in.putExtra("Tujuan",lokasiTujuan);
                    in.putExtra("TanggalBerangkat",berangkat);
                    in.putExtra("TanggalPulang",pulang);
                    in.putExtra("jumlahTiket",String.valueOf(jumlah));
                    startActivity(in);
                }
            }});

        tanggalview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerClass(); }});

        waktuview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerClass(); }});
        waktuviewpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerPP();
            }
        });
        tanggalviewpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDatePickerPP();
            }
        });
        //end waktu dan tanggal
        //Topup
        topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topupDialog();
                Toast.makeText(MainActivity.this, "TEST", Toast.LENGTH_SHORT).show();
            }
        });
        //endTopup
        //Spinner
        spinner = (Spinner)findViewById(R.id.spinner);
        List<String> catagories = new ArrayList<>();
        catagories.add(0,"Pilih");
        catagories.add("Jakarta (Rp 85.000)");
        catagories.add("Bekasi (Rp 100.000)");
        catagories.add("Cirebon (Rp 70.000)");

        ArrayAdapter<String> dataAdapter;
        dataAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, catagories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(parent.getItemAtPosition(position).equals("Pilih")){
                    Toast.makeText(MainActivity.this, "Pilih Daerah Tujuan", Toast.LENGTH_SHORT).show();
                }else if (parent.getItemAtPosition(position).equals("Jakarta (Rp 85.000)")){
                    lokasiTujuan = "Jakarta";
                    strHarga = "85000";
                } else if(parent.getItemAtPosition(position).equals("Bekasi (Rp 100.000)")){
                    lokasiTujuan = "Bekasi";
                    strHarga = "100000";
                }else{
                    lokasiTujuan = "Cirebon";
                    strHarga = "70000";
                    Toast.makeText(MainActivity.this, strHarga, Toast.LENGTH_SHORT).show();
                } }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }});
        //endSpinner

        //SwitchPP

        switchpp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true ){
                    tanggalpp.setText("Pilih Tanggal");
                    jampp.setText("Pilih Waktu");
                    waktuviewpp.setClickable(true);
                    tanggalviewpp.setClickable(true);
                }else{
                    tanggalpp.setText(null);
                    jampp.setText(null);
                    waktuviewpp.setClickable(false);
                    tanggalviewpp.setClickable(false);
                }
            }
        });

    }//End Switch
    //topup class
    public void topupDialog(){
        final EditText inputTopup = new EditText(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("TOP UP");
        builder.setMessage("Masukan Jumlah Top Up");
        builder.setCancelable(false);
        builder.setView(inputTopup);

        builder.setNeutralButton("Top Up", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(jumlahSaldo.getText().toString().equals("0") && saldo == 0){
                    jumlahSaldo.setText(inputTopup.getText());
                    saldo = Integer.parseInt(jumlahSaldo.getText().toString());
                }else{
                    top = inputTopup.getText().toString();
                    tops = Integer.parseInt(top);
                    saldo = (saldo + tops);
                    jumlahSaldo.setText(String.valueOf(saldo));
                }

                saldo = Integer.parseInt(jumlahSaldo.getText().toString());
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }//end topupclass
    // class datepicker
    public void datePickerClass (){
        cl = Calendar.getInstance();
        int day = cl.get(Calendar.DAY_OF_MONTH);
        int month = cl.get(Calendar.MONTH);
        int year = cl.get(Calendar.YEAR);
        datePicker = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                tanggalview.setText(mDay +"/"+(mMonth+1)+"/"+mYear);
            }
        },day,month,year);
        datePicker.show();
    }
    // class time picker
    public void timePickerClass (){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                waktuview.setText( String.format("%02d:%02d", selectedHour, selectedMinute));
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
    public void setDatePickerPP(){
        cl = Calendar.getInstance();
        int day = cl.get(Calendar.DAY_OF_MONTH);
        int month = cl.get(Calendar.MONTH);
        int year = cl.get(Calendar.YEAR);
        datePicker = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                tanggalpp.setText(mDay +"/"+(mMonth+1)+"/"+mYear);
            }
        },day,month,year);
        datePicker.show();
    }
    public void timePickerPP (){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                jampp.setText( String.format("%02d:%02d", selectedHour, selectedMinute));
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void beli(){

    }
}
