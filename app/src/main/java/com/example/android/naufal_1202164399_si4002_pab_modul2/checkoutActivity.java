package com.example.android.naufal_1202164399_si4002_pab_modul2;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class checkoutActivity extends AppCompatActivity {
    TextView tujuan,tanggalKonfirmasi,tanggalpp,jumlahTiket,totalHarga;
    Button btnBeli;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        final String ucapan = "terimakasih! Semoga Selamat Sampai Tujuan";
        tujuan = (TextView)findViewById(R.id.tujuan);
        tanggalKonfirmasi = (TextView)findViewById(R.id.tanggalKonfirmasi);
        tanggalpp = (TextView)findViewById(R.id.tglPulang);
        jumlahTiket = (TextView)findViewById(R.id.jumlahTiketKonfirmasi);
        totalHarga = (TextView)findViewById(R.id.totalHarga);
        btnBeli = (Button)findViewById(R.id.buttonBeli);
        tujuan.setText(getIntent().getStringExtra("Tujuan"));
        tanggalKonfirmasi.setText(getIntent().getStringExtra("TanggalBerangkat"));
        tanggalpp.setText(getIntent().getStringExtra("TanggalPulang"));
        jumlahTiket.setText(getIntent().getStringExtra("jumlahTiket"));
        totalHarga.setText(getIntent().getStringExtra("TotalHarga"));
        btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(checkoutActivity.this,MainActivity.class);
                startActivity(in);
                in.putExtra("ucapan",ucapan);
            }
        });
    }
}

